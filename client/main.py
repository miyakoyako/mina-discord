import discord
import requests

knownUserIDs = {}
knownChannelIDs = {}

class MyClient(discord.Client):
    async def on_ready(self):
        print('Logged on as', self.user)

    # On any socket message
    async def on_socket_response(self, msg):
        print(msg)

    async def on_message(self, discordMessage):
        try:
            # Check if message is from self
            if discordMessage.author.id != self.user.id:
                return
        
            # Check if the message starts with $query
            if discordMessage.content.startswith("$query"):
                # Get the core
                core = discordMessage.content.split(' ')[1]

                params = discordMessage.content.split(' ')[2]

                if core == 'user':
                    core = 'DiscordUsers'
                elif core == 'message':
                    core = 'DiscordMessages'
                elif core == 'discord':
                    core = 'MInaDiscord'
                
                # Get the query
                query = ' '.join(discordMessage.content.split(' ')[3:])

                if 'wt=' not in params:

                    # query upstream
                    r = requests.get(f'http://10.0.5.1:8981/solr/{core}/select?q={query}&indent=true&wt=csv&{params}')
                else:
                    r = requests.get(f'http://10.0.5.1:8981/solr/{core}/select?q={query}&indent=true&{params}')

                # Send the response (truncated to 2000 characters)
                outmessage = "```" + '\n\n'.join(r.text.replace('```', '[TILDA]').split('\n'))[:1990] + "```"
                await discordMessage.channel.send(outmessage)
                

        except Exception as e:
            print(e)
            return


client = MyClient()
client.run('token')